__author__ = 'Shelby'

DAILIES = [
    {
        "print":"Giant Jelly",
        "url":"http://www.neopets.com/jelly/jelly.phtml",
        "button_text":"Grab some Jelly"
    },
    {
        "print":"Giant Omelette",
        "url":"http://www.neopets.com/prehistoric/omelette.phtml",
        "button_text":"Grab some Omelette"
    },
    {
        "print":"Tombola",
        "url":"http://www.neopets.com/island/tombola.phtml",
        "button_text":"Play Tombola!"
    },
    {
        "print":"Desert Shrine",
        "url":"http://www.neopets.com/desert/shrine.phtml",
        "button_text":"Approach the Shrine"
    },
]
