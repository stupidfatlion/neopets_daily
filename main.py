__author__ = 'Shelby'

from selenium import webdriver
from time import sleep, time
import CREDENTIALS
import BeautifulSoup
from DAILIES import DAILIES
from random import randint
from progressbar import ProgressBar

class bot():
    def __init__(self):
        self.last_purchased = None
        self.percentage_threshold = 140

    def _click_submit(self, text):
        self.driver.find_element_by_xpath('//input[@type="submit" and @value="%s"]' %(text)).click()

    def login(self):
        self.driver.get("http://www.neopets.com")
        self.driver.find_element_by_link_text("Log in").click()
        form_username = self.driver.find_element_by_name("username")
        form_username.send_keys(CREDENTIALS.USERNAME)
        form_password = self.driver.find_element_by_name("password")
        form_password.send_keys(CREDENTIALS.PASSWORD)
        self._click_submit("Log In!")


    def buy_stocks(self):
        print "Going shopping..."
        market_url = "http://www.neopets.com/stockmarket.phtml?type=list&full=true"
        self.driver.get(market_url)
        soup = BeautifulSoup.BeautifulSoup(self.driver.page_source)
        for entry in soup.find("table", align="center").findAll("tr")[1:]:
            if self.last_purchased:
                now = time()
                difference = (now - self.last_purchased)
                print difference
                if difference < 86400:
                    print "Waiting for some time to buy again"
                    break
            for index, part in enumerate(entry.findAll("td")):
                if index == 1:
                    stock = part.text
                elif index == 5:
                    stock_value = int(part.text)
                    if stock_value==15 or stock_value==16:
                        print "Buying ", stock
                        self.driver.get("http://www.neopets.com/stockmarket.phtml?type=buy")
                        ticker_symbol_form = self.driver.find_element_by_name("ticker_symbol")
                        ticker_symbol_form.send_keys(stock)
                        number_of_shares_form = self.driver.find_element_by_name("amount_shares")
                        number_of_shares_form.send_keys("1000")
                        self._click_submit("Buy Shares")
                        self.last_purchased = time()
                        break


    def sell_stocks(self):
        print "Selling stocks..."
        portfolio_url = "http://www.neopets.com/stockmarket.phtml?type=portfolio"
        self.driver.get(portfolio_url)
        sell_these = {}
        #expand them all..
        image_url = "http://images.neopets.com/stockmarket/disclosure_closed.gif"
        for arrow in self.driver.find_elements_by_xpath("//img[@src='%s']" %(image_url)):
            arrow.click()
        soup = BeautifulSoup.BeautifulSoup(self.driver.page_source)
        #find all tables
        for table in soup.findAll("table")[1:]:
            if "Species" not in table.text and "Sell" in table.text:
                for row in table.findAll("tr")[1:]:
                    shares = 0
                    change = 0
                    sell = False
                    for index, entry in enumerate(row.findAll("td")):
                        if index == 0:
                            shares = entry.text
                        elif index == 5:
                            try:
                                change = float(entry.text.replace("%", ""))
                                if change>120:
                                    sell = True
                            except:
                                change = 0
                        elif index == 6 and sell:
                            sell_these[entry.find("input")["name"]] = shares
        for stock in sell_these:
            sell_field = self.driver.find_element_by_name(stock)
            sell_field.send_keys(sell_these[stock].strip().replace(",", ""))
            print "Selling ", sell_field, sell_these[stock]
        if sell_these:
            self._click_submit("Sell Shares")

    def _do_daily(self, statement, url, text):
        print statement
        self.driver.get(url)
        self._click_submit(text)

    def dailies(self):
        for daily in DAILIES:
            try:
                self._do_daily(daily["print"], daily["url"], daily["button_text"])
            except:
                print "Failed, probably not available"

    def battle_dome(self):
        #TODO do battledome
        pass

    def main(self):
        while True:
            self.driver = webdriver.Chrome()
            self.login()
            self.dailies()
            self.buy_stocks()
            self.sell_stocks()
            sleep(5)
            self.driver.quit()
            print "Sleeping...."
            sleep_time = randint(3600, 4000)
            pbar = ProgressBar()
            for i in pbar(xrange(sleep_time,0,-1)):
                sleep(1)



if __name__ == "__main__":
    b = bot()
    b.main()